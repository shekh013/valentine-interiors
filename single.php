<?php
/**
* The template for displaying all single posts
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
*
* @package Azcods_Theme
*/

get_header(); ?>

  <div class="container">
    <div class="row">
      <div class="col-sm-9">
        <div id="primary" class="content-area">
          <main id="main" class="site-main blog-base" role="main">

            <?php
              while ( have_posts() ) : the_post();

              get_template_part( 'layouts/content', get_post_format() );



              // If comments are open or we have at least one comment, load up the comment template.
              if ( comments_open() || get_comments_number() ) :
                  comments_template();
              endif;

              endwhile; // End of the loop.
              ?>

          </main>
          <!-- #main -->
        </div>
        <!-- #primary -->

      </div>

      <div class="col-sm-3 blog-sidebar">
        <?php get_sidebar(); ?>
      </div>

    </div>
  </div>





  <?php get_footer();