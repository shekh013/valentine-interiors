<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Azcods_Theme
 */

?>

	</div><!-- #content -->

<footer id="main-footer" class="site-footer"><!--FOOTER BG COLOR -->
    <div class="container main-footer-area">

        <div class="row">
            <div class="col-sm-3 column-1">

                <div class="footer-logo">
                    <?php
                            $logo_img = get_field('logo_header', 'option');

                            if (!empty($logo_img)):
                                ?>



                                <a href="<?php echo esc_url(home_url('/')); ?>"><img class="img-responsive"  src="<?php echo $logo_img['url']; ?>" alt="<?php echo $logo_img['alt']; ?>" /></a>
                                <?php else : {
                                    ?>
                                <h1 class="site-title">
                                    <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"
                                        rel="home">
                                        <?php bloginfo('name'); ?>
                                    </a>
                                </h1>
                                <?php
                                }
                            endif;
                            ?>
                    </div>

                    <div class="footer-contact-info">
                    <ul>
                        <li class="phone"><?php the_field('phone_footer', 'option');?></li>
                        <li class="email"><?php the_field('email_footer', 'option');?></li>
                    </ul>
                    </div>

                    <div class="footer-social-share">
                        <ul>
                        <li class=""><a target="_blank" href="<?php the_field('facebook_url','option')?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb.png"/></a></li>
                        <li class=""><a target="_blank" href="<?php the_field('instagram_url','option')?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ins.png"/></a></li>
                        <li class=""><a target="_blank" href="<?php the_field('pinterest_url','option')?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/pin.png"/></a></li>
                        <li class=""><a target="_blank" href="<?php the_field('houzz_url','option')?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/hou.png"/></a></li>
                        <li class=""><a target="_blank" href="<?php the_field('linkedin_url','option')?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/lin.png"/></a></li>
                    </ul>
                    </div>



            </div>

            <div class="col-sm-5 column-2">
                <h3 class="footer-sec-title">blog</h3>
                <?php skh_footer_blog_posts() ?>

            </div>

            <div class="col-sm-4 column-3">
                 <h3 class="footer-sec-title">instagram feed</h3>
                 <!-- LightWidget WIDGET --><script src="//lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/6358ac4854bb52f586fdbba2d0a88ea1.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>

            </div>
        </div>

    </div><!-- close .container -->

    <div class="footer-copyinfo">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <P><?php the_field('copy_right_info', 'option');?></p>
                </div>

            </div>
        </div>
    </div>
</footer><!-- close #colophon -->




</div><!-- #page -->


<div id="image-pop-wrapper">


<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()"> x </span>

  <div class="modal-content">

        <?php
        $gallery = get_field('gallery_of_work');
        if( $gallery ): ?>
        <?php foreach( $gallery as $image ): ?>
            <div class="mySlides">

                <img class="img-responsive" src="<?php echo $image['url'];?>">
            </div>
        <?php endforeach;?>
        <?php endif;
        ?>

    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>



  </div>
</div>
</div>


<script type="text/javascript">


var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);

}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName('mySlides');

  var dots = document.getElementsByClassName('demo');
  var captionText = document.getElementsByClassName("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  // for (i = 0; i < dots.length; i++) {
  //   dots[i].className = dots[i].className.replace(" active", "");
  // }
  slides[slideIndex-1].style.display = "block";
  //dots[slideIndex-1].className += " active";
  //captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
<?php wp_footer(); ?>

  <script type="text/javascript">

  var openModal;
  var closeModal;
    jQuery(document).ready(function($) {

openModal = function() {
  document.getElementById('myModal').style.display = "block";
$('body').addClass("img-modal");

}

closeModal = function() {
  document.getElementById('myModal').style.display = "none";
  $('body').removeClass("img-modal");
}



    //   $(window).scroll(function() {
    //     var scroll = $(window).scrollTop();
    //     if (scroll >= 1) {
    //       $('.site-navigation').addClass("sticky-navbar");

    //     } else {
    //       $('.site-navigation').removeClass("sticky-navbar");
    //     }
    //   });

    });
  </script>

</body>
</html>
