<?php
/**
* The template for displaying Shop Single page
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
*
* @package Azcods_Theme
*/

get_header(); ?>




  <section class="project-single__header">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">

          <?php
            $image_project = get_field('featured_image_product');
            if( !empty($image_project) ): ?>
                <div class="et_pb_module et-waypoint et_pb_image et_pb_animation_off et_pb_image_0 et_always_center_on_mobile et_pb_has_overlay et-animated">
                    <a href="<?php echo $image_project['url']; ?>" class="et_pb_lightbox_image" title="">
                    <img class="img-responsive" src="<?php echo $image_project['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <span class="et_overlay et_pb_inline_icon" data-icon="P"></span>
                    </a>
                </div>
            <?php endif; ?>
        </div>

        <div class="col-sm-7">

          <h3><?php the_title();?></h3>
          <h1><?php the_field('price_product');?></h1>
          <p>
            <?php the_field('description_product');?>
          </p>

          <a href="<?php the_field('product_url_product')?>" class="btn-sk btn-sk-nm btn-shop"> Read More</a>
        </div>

      </div>
    </div>
  </section>



  <?php get_footer();