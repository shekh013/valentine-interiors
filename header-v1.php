<header id="masthead-v1">

    <nav class="site-navigation sticky-navbar">
        <div class="container-fluid" style="padding-bottom: 0px;">
            <div class="row">
                <div class="site-navigation-inner col-sm-12" style="padding:0px;">
                    <div class="navbar navbar-default">
                        <div class="navbar-header">
                            <!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <!-- Your site title as branding in the menu -->
                            <?php
                            $logo_img = get_field('logo_header', 'option');
                            if (!empty($logo_img)):
                                ?>
                                <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"><img class="img-responsive"  src="<?php echo $logo_img['url']; ?>" alt="<?php echo $logo_img['alt']; ?>" /></a>
                                <?php else : {
                                    ?>
                                <h1 class="site-title">
                                    <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"
                                        rel="home">
                                        <?php bloginfo('name'); ?>
                                    </a>
                                </h1>
                                <?php
                                }
                            endif;
                            ?>

                        </div>

                        <!-- The WordPress Menu goes here -->
                        <?php
                        wp_nav_menu(
                                array(
                                    'theme_location' => 'header_menu',
                                    'container_class' => 'collapse navbar-collapse navbar-responsive-collapse',
                                    'menu_class' => 'nav navbar-nav navbar-right',
                                    'fallback_cb' => '',
                                    'menu_id' => 'main-menu'
                                )
                        );
                        ?>

                    </div>
                    <!-- .navbar -->
                </div>
            </div>
        </div>
        <!-- .container -->
    </nav>
    <!-- .site-navigation -->

</header>
<!-- #masthead -->