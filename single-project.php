<?php

/**
* The template for displaying Project Single page
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
*
* @package Azcods_Theme
*/


get_header();
?>




  <section class="project-single__header">
    <div class="container">
      <div class="row">
        <div class="col-sm-5">

          <?php
$image_project = get_field('features_image_of_work');

if( !empty($image_project) ): ?>
                <div class="et_pb_module et-waypoint et_pb_image et_pb_animation_off et_pb_image_0 et_always_center_on_mobile et_pb_has_overlay et-animated">
                    <a href="<?php echo $image_project['url'];?>" class="et_pb_lightbox_image" title="">
                    <img class="img-responsive" src="<?php echo $image_project['url'];?>" alt="<?php echo $image['alt'];?>" />
                    <span class="et_overlay et_pb_inline_icon" data-icon="P"></span>
                    </a>
                </div>
            <?php endif;
?>
        </div>

        <div class="col-sm-7">

          <h3><?php the_field('caption_of_work');
?></h3>
          <h1><?php the_title();
?></h1>
          <p>
            <?php the_field('description_of_work');
?>
          </p>
        </div>

      </div>
    </div>
  </section>








  <section class="project-single__gallery">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">

                <?php
                $gallery = get_field('gallery_of_work');

                if( $gallery ): ?>

                  <ul class="">
                  <?php $j =1;?>
                  <?php foreach( $gallery as $image ): ?>

                      <li class="">

          <!--<div class="et_pb_module et-waypoint et_pb_image et_pb_animation_top et_pb_image_0 et_always_center_on_mobile et_pb_has_overlay et-animated">
              <a href="<?php echo $image['url'];?>" class="et_pb_lightbox_image" title="">
              <img class="img-responsive" src="<?php echo $image['sizes']['work-thumbnails-gallery'];?>" alt="<?php echo $image['alt'];?>" />
              <span class="et_overlay et_pb_inline_icon" data-icon="T"></span></a>
              </a>
          </div>-->
                      <img class="img-responsive" src="<?php echo $image['sizes']['work-thumbnails-gallery-square'];?>" alt="<?php echo $image['alt'];?>" onclick="openModal();currentSlide(<?php echo $j?>)"/>
                      </li>
                      <?php $j++; ?>
                      <?php endforeach;?>
                  </ul>
                <?php endif;
                ?>




        </div>
      </div>
    </div>
  </section>






  <?php get_footer('single-project');





