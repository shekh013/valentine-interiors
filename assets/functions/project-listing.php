<?php


function skh_projects_listing() {

	    $args = array(
        'post_type' => 'project',
        'posts_per_page' => 999,
    );
    $main_post = new WP_Query($args);
    ?>

        <ul class="project-card">
            <?php while ($main_post->have_posts()) : $main_post->the_post(); ?>


                    <li class="project-card__wrapper">

                        <?php
                        $project_bg = get_field('features_image_of_work');
                        ?>

                        <div class="project-card__item" style="background-image:url(<?php echo $project_bg['url']; ?>)">
                                <div class="project-card__description">
                                        <ul class="list-unstyled list-inline">
                                            <li><h6 class="project-card__caption"><?php the_field('caption_of_work'); ?></h6>
                                        <a href="<?php the_permalink(); ?>" rel="bookmark">
                                            <h4 class="project-card__title"><?php the_title(); ?></h4>
                                        </a></li>
                                            <li> <a class="btn-sk" href="<?php the_permalink(); ?>" >View</a></li>
                                        </ul>
                                </div>
                        </div>

                    </li>


            <?php endwhile; ?>
    </ul>

<?php

}





add_shortcode( 'sk_sortable_integ_page', 'shortcode_sk_sortable_integ_page' );

/* Shortcode function */
function shortcode_sk_sortable_integ_page( $atts , $content = null ) {
ob_start();

    extract( shortcode_atts(
        array(
            'posts'         => '999',
            'post_type'     => 'post',
            'include'       => '',
            'filter'        => 'yes',
        ), $atts )
    );


    if ($include && $filter == 'yes') {
        $included_terms = explode( ',', $include );
        $included_ids = array();

        foreach( $included_terms as $term ) {
            $term_id = get_term_by( 'slug', $term, 'project_category')->term_id;
            $included_ids[] = $term_id;
        }
        $id_string = implode( ',', $included_ids );
        $terms = get_terms( 'project_category', array( 'include' => $id_string ) );
         ?>


<div class="sortable-list__wrapping">

            <?php //Build the filter ?>
            <div class="filter-projects">
                <ul class="project-filter list-unstyled list-inline" id="filters">
                    <li> <a href="#" data-filter="*">All</a></li>
                        <?php $count = count($terms);
                            if ( $count > 0 ){
                            foreach ( $terms as $term ) { ?>
                    <li><a href='#' data-filter=.<?php echo $term->slug ?> > <?php echo $term->name?> </a></li>
                            <?php } }?>
                </ul>
            </div>


            <?php //Build the layout  ?>
    <ul class="project-card ortable-list__box" id="isotope-container" data-portfolio-effect="fadeInUp">
        <?php
        $the_query = new WP_Query( array ( 'post_type' => $post_type, 'posts_per_page' => $posts ) );
        while ( $the_query->have_posts() ):
        $the_query->the_post();
        global $post;
        $id = $post->ID;
        $termsArray = get_the_terms( $id, 'project_category' );
        $termsString = "";

        if ( $termsArray) {
            foreach ( $termsArray as $term ) {
                $termsString .= $term->slug.' ';
            }
        } ?>

        <li class="project-card__wrapper isotope-item <?php echo $termsString; ?> ">

                        <?php
                        $project_bg = get_field('features_image_of_work');
                        // thumbnail
                        $size = 'work-thumbnails';
                        $thumb = $project_bg['sizes'][ $size ];

                        ?>


                            <div class="project-card__item">

                                    <div class="project-card__description">

                                        <ul class="list-unstyled list-inline">

                                            <li><h6 class="project-card__caption"><?php the_field('caption_of_work'); ?></h6>
                                            <a href="<?php the_permalink(); ?>" rel="bookmark">
                                            <h4 class="project-card__title"><?php the_title(); ?></h4>
                                            </a></li>
                                            <li> <a class="btn-sk" href="<?php the_permalink(); ?>" >View</a> </li>

                                        </ul>

                                    </div>
                                    <a class="link--cover" href="<?php the_permalink(); ?>"></a>
                                    <img class="img-responsive project-card__image" src="<?php echo $thumb; ?>" />
                            </div>




        </li>
    <?php endwhile; ?>
    </ul>

<?php }?>


</div>


    <?php
    $content_data = ob_get_contents();
    ob_end_clean();
    return $content_data;
}

//add_filter( 'widget_text', 'do_shortcode');

/*
 * Sample Shortcode to out put the HTML
 *
 * [sk_sortable_integ_page posts="8" show_all_text="See all" post_type="projects" filter="yes" include=""][/sk_sortable_integ_page]
 *
 */




 add_shortcode('project_home_widget', 'shortcode_project_home_widget');

function shortcode_project_home_widget($atts, $content = null) {

    ob_start();
?>

<div class="latest-project-container">

<?php
$post_objects = get_field('project_to_display_on_homepage', option);

if( $post_objects ): ?>
    <ul class="latest-project__list">
    <?php foreach( $post_objects as $post_object): ?>
        <li class="latest-project__item">


            <?php
            $project_bg = get_field('features_image_of_work',$post_object->ID);
                                    // thumbnail
                        $size = 'work-thumbnails';
                        $thumb = $project_bg['sizes'][ $size ];
            ?>


                    <div class="latest-project__description">

                            <h6 class="project-card__caption"><?php the_field('caption_of_work',$post_object->ID); ?></h6>
                            <a href="<?php the_permalink($post_object->ID); ?>" rel="bookmark">
                                <h4 class="project-card__title"><?php echo get_the_title($post_object->ID); ?></h4>
                            </a>


                    </div>
                    <img class="img-responsive project-card__image" src="<?php echo $thumb; ?>" />


        </li>
    <?php endforeach; ?>
    </ul>
<?php endif;

?>

</div>


<?php

    $content_data = ob_get_contents();
    ob_end_clean();
    return $content_data;
}

/** [project_home_widget][/project_home_widget]  **/