<?php
function my_custom_posttypes() {

/* ========================Shop======================= */
	    $labels_shop = array(
        'name'               => 'Shop',
        'singular_name'      => 'Shop',
        'menu_name'          => 'Shop',
        'name_admin_bar'     => 'Shop',
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New Shop Item',
        'new_item'           => 'New Shop Item',
        'edit_item'          => 'Edit Shop Item',
        'view_item'          => 'View Shop Item',
        'all_items'          => 'All Shop Item',
        'search_items'       => 'Search Shop Item',
        'parent_item_colon'  => 'Parent Shop Item:',
        'not_found'          => 'No Shop Item found.',
        'not_found_in_trash' => 'No Shop Item found in Trash.',
    );

    $args_connector = array(
        'labels'             => $labels_shop,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'menu_icon'          => 'dashicons-cart',
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'shop' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'supports'           => array('title'),

    );
    /* ========================shop======================= */


    register_post_type( 'shop', $args_connector);

}
add_action( 'init', 'my_custom_posttypes' );


// Flush rewrite rules to add "review" as a permalink slug
function my_rewrite_flush() {
    my_custom_posttypes();
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'my_rewrite_flush' );