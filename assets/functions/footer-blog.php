<?php

// Related Posts Function (call using skh_footer_blog_posts(); )
function skh_footer_blog_posts() {

	    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 999,
    );
    $main_post = new WP_Query($args);
    ?>

        <div class="post-card">
            <?php while ($main_post->have_posts()) : $main_post->the_post(); ?>


                    <div class="post-card__item">

                        <div class="img-resposnsive post-card__thumb">
                            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo the_post_thumbnail('thumbnail'); ?></a>
                        </div>

                        <div class="post-card__description">
                        	<a href="<?php the_permalink(); ?>" rel="bookmark">
                        		<h5 class="post-card__title"><?php the_title(); ?></h5>
                            </a>
                            <div class="post-card__excerpt">
                                <?php the_excerpt()?>
                            </div>


                            <p class="post-card__date"><?php the_time('F jS, Y') ?></p>
                        </div>



                    </div>


            <?php endwhile; ?>
    </div>





	<?php wp_reset_query();
}



