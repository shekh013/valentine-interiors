<?php
/**
 * Template Name:Shop
 *
 *
 */

get_header(); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();
				the_content();
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->


<div class="shop-wrapper">




<div class="container">
    <div class="row">
        <div class="col-sm-12">

            <?php
                $args = array(
                    'post_type' => 'shop',
                    'posts_per_page' => 999,
                );
                $main_post = new WP_Query($args);
                ?>

                <ul class="shop-container">
                 <?php while ($main_post->have_posts()) : $main_post->the_post(); ?>

                    <?php $image_shop = get_field('featured_image_product'); ?>

                    <li class="shop-container__item">
                        <?php
                        $link = get_field('product_url_product');
                        if($link):
                        $link_show = 'href="'.$link. '"';
                        endif;
                        ?>

                        <a <?php echo $link_show;?> target="_blank">
                        <div class="shop-container__product">

                        <?php
                        if( !empty($image_shop) ): ?>
                            <img class="img-responsive" src="<?php echo $image_shop['url']; ?>" alt="<?php echo $image_shop['alt']; ?>" />
                        <?php endif; ?>

                        <div class="shop-container__product-info">
                        <h4><span><?php the_title() ?></span> <?php the_field('price_product') ?></h4>
                        <p><?php the_field('description_product') ?></p>

                        </div>

                        </div>
                        </a>
                    </li>

                  <?php endwhile; ?>
                </ul>


        </div>
    </div>

</div>

</div>

<?php get_footer();