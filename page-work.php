<?php
/**
* Template Name:Work Page
*
*
*/

get_header(); ?>


<?php $image_bg = get_field('banner_work_page'); ?>


  <section class="work-page" style="background-image:url(<?php echo $image_bg['url']; ?>);">

  </section>

  <section>
    <div class="section-title-global custom-margin">
      <h1><?php the_field('section_title_work_page');?></h1>
    </div>

  </section>

<section class="project-listing-page">
<?php //skh_projects_listing(); ?>
<?php echo do_shortcode('[sk_sortable_integ_page posts="40" show_all_text="See all" post_type="project" filter="yes" include="all"][/sk_sortable_integ_page]')?>
</section>




  <?php get_footer();